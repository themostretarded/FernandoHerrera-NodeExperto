const fs = require('fs')
//al añadir async se retorna una promesa en explicito
const crearArchivo = async (base = 5) => {
    try {
        console.log("================");
        console.log('Tabla del:', base);
        console.log("================");
        let salida = "";
        for (i = 1; i <= 10; i++) {
            salida += `${base} X ${i} = ${base * i}\n`;
        }
        console.log(salida)
        fs.writeFileSync(`tabla-${base}.txt`, salida);
        return `tabla-${base}.txt`;
        // fs.writeFile(`tabla-${base}.txt`,salida,(err)=>{
        // if(err)throw err;
        // console.log(`tabla-${base} creado`)
        // })
    }
    catch (err) {
        throw (err)
    }

}

module.exports = {
    crearArchivo: crearArchivo
}


// forma con return promise
// const crearArchivo = (base = 5) => {
//     return new Promise((resolve, reject) => {
//         console.log("================");
//         console.log('Tabla del:', base);
//         console.log("================");
//         let salida = "";
//         for (i = 1; i <= 10; i++) {
//             salida += `${base} X ${i} = ${base * i}\n`;
//         }
//         console.log(salida)
//         fs.writeFileSync(`tabla-${base}.txt`, salida);
//         resolve(`tabla-${base}`);
//         // fs.writeFile(`tabla-${base}.txt`,salida,(err)=>{
//         // if(err)throw err;
//         // console.log(`tabla-${base} creado`)
//         // })
//     })

// }