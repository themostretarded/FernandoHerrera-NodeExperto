const empleados =[
    {
        id:1,
        nombre: 'Marcos'
    },
    {
        id:2,
        nombre: 'karen'
    },
    {
        id:3,
        nombre: 'linda'
    }
];

const salarios =[
    {
        id:1,
        salario: 1000
    },
    {
        id:2,
        salario: 6000
    }
]


const id = 4;

const getEmpleado = (id)=>{
    return new Promise((resolve,reject)=>{
        const empleado = empleados.find((e)=>e.id ===id)?.nombre;
        //if simplificado
        (empleado)
        ? resolve(empleado)
        : reject(`No existe empelado con id ${id}`);  
    });
}

const getSalario =(id)=>{
    return new Promise((respuesta,rechazo)=>{
        const salario = salarios.find((s)=>s.id===id)?.salario;
        (salario)
        ? respuesta(salario)
        : rechazo(`No existe salario para id: ${id}`)
    })
}

// getEmpleado(id).then(empleado => console.log(empleado))
//                 .catch(err=> console.log(err));

// getSalario(id).then(salario => console.log(salario))
//                 .catch(err=> console.log(err));

// el horror
// getEmpleado(id).then(empleado=>{
//     getSalario(id).then(salario=>{
//         console.log('El empleado:', empleado,' Tiene un salario de: ',salario);
//     })
//     .catch(err => console.log(err))
// })
// .catch(err => console.log(err))
let nombre;

getEmpleado(id)
.then(empleado =>{
    nombre = empleado
    return getSalario(id);
})
.then(salario =>console.log('El empleado:', nombre,' tiene un salario: ',salario))
.catch(err =>console.log(err))