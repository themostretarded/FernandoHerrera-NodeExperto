const empleados =[
    {
        id:1,
        nombre: 'Marcos'
    },
    {
        id:2,
        nombre: 'karen'
    },
    {
        id:3,
        nombre: 'linda'
    }
];

const salarios =[
    {
        id:1,
        salario: 1000
    },
    {
        id:2,
        salario: 6000
    }
]

const id = 4;

const getEmpleado = (id)=>{
    return new Promise((resolve,reject)=>{
        const empleado = empleados.find((e)=>e.id ===id)?.nombre;
        //if simplificado
        (empleado)
        ? resolve(empleado)
        : reject(`No existe empelado con id ${id}`);  
    });
}

const getSalario =(id)=>{
    return new Promise((respuesta,rechazo)=>{
        const salario = salarios.find((s)=>s.id===id)?.salario;
        (salario)
        ? respuesta(salario)
        : rechazo(`No existe salario para id: ${id}`)
    })
}

const getInfoUsuario = async(id) =>{
    try{
        const empleado = await getEmpleado(id);
        const salario = await getSalario(id);
        return `El salario del empleado: ${empleado} es de ${ salario}`;
    }
    catch(err){
        throw err;
    }
    
}

getInfoUsuario(id).then(msg => {
    console.log("todo bien")
    console.log(msg)})
                    .catch(err => {
                        console.log("valio madre")
                        console.log(err)});